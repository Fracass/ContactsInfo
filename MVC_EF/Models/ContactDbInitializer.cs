﻿using MVC_EF.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVC_EF.Models
{
    public class ContactDbInitializer : DropCreateDatabaseAlways<ContactModel>
    {
        /// <summary>
        /// Тестовые данные
        /// </summary>
        /// <param name="db"></param>
        protected override void Seed(ContactModel db)
        {
            
            db.tblContacts.Add(new tblContacts { Name = "Иван", Lname = "Иванов", Tname = "Иванович", DateOfBirth = Convert.ToDateTime("20.10.1980"), Gender = "Мужской", Age = Convert.ToInt32(tblContactsController.Age(Convert.ToDateTime("20.10.1980"))) });
            db.tblContacts.Add(new tblContacts { Name = "Шорин", Lname = "Алексей", Tname = "Алексеевич", DateOfBirth = Convert.ToDateTime("20.10.1999"), Gender = "Мужской", Age = Convert.ToInt32(tblContactsController.Age(Convert.ToDateTime("20.10.1999"))) });
            db.tblContacts.Add(new tblContacts { Name = "Федор", Lname = "Петров", Tname = "Михайлович", DateOfBirth = Convert.ToDateTime("20.10.1940"), Gender = "Мужской", Age = Convert.ToInt32(tblContactsController.Age(Convert.ToDateTime("20.10.1940"))) });

            base.Seed(db);
        }
    }
}