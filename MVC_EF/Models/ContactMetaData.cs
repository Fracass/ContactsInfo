﻿using MVC_EF.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MVC_EF.Models
{
    /// <summary>
    /// Метаданные сущности контаков
    /// </summary>
    public class ContactMetaData
    {
        [Required]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required]        
        [Display(Name = "Фамилия")]
        public string Lname { get; set; }

        [Required]
        [Display(Name = "Отчество")]
        public string Tname { get; set; }

        [Required]
        [Display(Name = "Дата рождения")]
        [DataRange("01/01/1900")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode=true)]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        [Display(Name = "Пол")] 
        public string Gender { get; set; }

        [ReadOnly(true)]
        [Display(Name = "Возраст")]
        public int Age { get; set; }
    }

    //применение аттрибутов привязано к частичному классу tblContatcts, чтобы при пересоздании БД 
    //метаданные не терялись
    [MetadataType(typeof(ContactMetaData))]
    public partial class tblContacts
    {

    }
}