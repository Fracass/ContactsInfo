namespace MVC_EF.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// ������������������� ����� �� ������ ��
    /// </summary>
    public partial class tblContacts
    {
        public int Id { get; set; }
        
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Lname { get; set; }

        [StringLength(100)]
        public string Tname { get; set; }

        public DateTime DateOfBirth { get; set; }

        [StringLength(10)]
        public string Gender { get; set; }
                
        public int? Age { get; set; }
    }       
}
