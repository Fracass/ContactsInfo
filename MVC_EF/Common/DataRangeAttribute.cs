﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_EF.Common
{
    public class DataRangeAttribute : RangeAttribute
    {
        //Установил верхнюю границу поля даты рождения, чтоббы нельзя было указать дату рождения позже сегодняшнего дня
        public DataRangeAttribute(string minimumValue) : base(typeof(DateTime), minimumValue, DateTime.Now.ToShortDateString())
        {

        }
    }
}