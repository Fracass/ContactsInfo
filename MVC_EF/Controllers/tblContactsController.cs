﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_EF.Models;

namespace MVC_EF.Controllers
{
    public class tblContactsController : Controller
    {
        private ContactModel db = new ContactModel();
        
        // GET: tblContacts
        public ActionResult Index(string sortBy)
        {
            ViewBag.SortNameParameter = string.IsNullOrEmpty(sortBy) ? "Name desc" : "";
            ViewBag.SortGenderParameter = string.IsNullOrEmpty(sortBy) ? "Gender desc" : "Gender";
            ViewBag.SortLnameParameter = string.IsNullOrEmpty(sortBy) ? "Lname desc" : "Lname";
            ViewBag.SortTnameParameter = string.IsNullOrEmpty(sortBy) ? "Tname desc" : "Tname";
            ViewBag.SortDobParameter = string.IsNullOrEmpty(sortBy) ? "Dob desc" : "Dob";
            ViewBag.SortAgeParameter = string.IsNullOrEmpty(sortBy) ? "Age desc" : "Age";
                       
            var contacts = db.tblContacts.AsQueryable();

            switch (sortBy)
            {
                case "Name desc":
                    contacts = contacts.OrderByDescending(x => x.Name);
                    break;
                case "Gender desc":
                    contacts = contacts.OrderByDescending(x => x.Gender);
                    break;
                case "Gender":
                    contacts = contacts.OrderBy(x => x.Gender);
                    break;
                case "Lname desc":
                    contacts = contacts.OrderByDescending(x => x.Lname);
                    break;
                case "Lname":
                    contacts = contacts.OrderBy(x => x.Lname);
                    break;
                case "Tname desc":
                    contacts = contacts.OrderByDescending(x => x.Tname);
                    break;
                case "Tname":
                    contacts = contacts.OrderBy(x => x.Tname);
                    break;
                case "Dob desc":
                    contacts = contacts.OrderByDescending(x => x.DateOfBirth);
                    break;
                case "Dob":
                    contacts = contacts.OrderBy(x => x.DateOfBirth);
                    break;
                case "Age desc":
                    contacts = contacts.OrderByDescending(x => x.Age);
                    break;
                case "Age":
                    contacts = contacts.OrderBy(x => x.Age);
                    break;
                default:
                    contacts = contacts.OrderBy(x => x.Name);
                    break;
            }

            return View(contacts);            
        }

        // GET: tblContacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblContacts tblContacts = db.tblContacts.Find(id);
            
            if (tblContacts == null)
            {
                return HttpNotFound();
            }
            return View(tblContacts);
        }

        // GET: tblContacts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tblContacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Lname,Tname,DateOfBirth,Gender")] tblContacts tblContacts)
        {

            //calculates the age using the dateOfBirth from text control 
            tblContacts.Age = Convert.ToInt32(Age(tblContacts.DateOfBirth));

            if (ModelState.IsValid)
            {
                db.tblContacts.Add(tblContacts);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblContacts);
        }

        // GET: tblContacts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblContacts tblContacts = db.tblContacts.Find(id);
            if (tblContacts == null)
            {
                return HttpNotFound();
            }
            return View(tblContacts);
        }

        // POST: tblContacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Lname,Tname,DateOfBirth,Gender")] tblContacts tblContacts)
        {
            tblContacts.Age = Convert.ToInt32(Age(tblContacts.DateOfBirth));

            if (ModelState.IsValid)
            {
                db.Entry(tblContacts).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblContacts);
        }

        // GET: tblContacts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblContacts tblContacts = db.tblContacts.Find(id);
            if (tblContacts == null)
            {
                return HttpNotFound();
            }
            return View(tblContacts);
        }

        // POST: tblContacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblContacts tblContacts = db.tblContacts.Find(id);
            db.tblContacts.Remove(tblContacts);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteMultiple(IEnumerable<int> contactIdsToDelete)
        {
            List<tblContacts> lst = db.tblContacts.Where(x => contactIdsToDelete.Contains(x.Id)).ToList();

            foreach (tblContacts contact in lst)
            {
                db.tblContacts.Remove(contact);
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public static string Age(DateTime birthday)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            if (now < birthday.AddYears(age)) age--;

            return age.ToString();
        }
    }
}
