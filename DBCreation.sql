use MVC go

CREATE TABLE [dbo].[tblContacts] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Name]        NCHAR (100)   NULL,
    [Lname]       NCHAR (100)   NULL,
    [Tname]       NCHAR (100)   NULL,
    [DateOfBirth] DATETIME      NULL,
    [Gender]      NVARCHAR (10) NULL,
    [Age]         INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

INSERT INTO tblContacts(Name, Lname, Tname, DateOfBirth, Gender, Age)
VALUES (N'����', N'����', N'��������', '11.02.1964', N'�������', 53);

INSERT INTO tblContacts(Name, Lname, Tname, DateOfBirth, Gender, Age)
VALUES (N'�����', N'������', N'���������', '11/05/1993', N'�������', 24);

INSERT INTO tblContacts(Name, Lname, Tname, DateOfBirth, Gender, Age)
VALUES (N'�����', N'�������', N'��������', '11.02.1996', N'�������', 21);


